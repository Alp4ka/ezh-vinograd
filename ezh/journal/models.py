
from django.db import models
from django.contrib.auth.models import AbstractUser
import datetime
import django.utils

class MyUser(AbstractUser):
    # Mutual fields
    type_user = models.IntegerField(default=0) # 0-Guest 1-Teacher 2-Student 3-Staff
    personal_code = models.CharField(max_length=128, default='')
    midname = models.CharField(max_length=128, default='')
    date_of_birth = models.DateField(auto_now_add=False, null=True)
    email = models.EmailField(max_length=128)
    telephone = models.CharField(max_length=128, default='')
    activated = models.BooleanField(default=False)
    # From teacher
    info_parents = models.CharField(max_length=228, default='')
    self_grades = models.ManyToManyField(to='Grade', related_name="self_grades_id")
    #all_lessons = models.ManyToManyField(to='Lesson_instance', related_name='subjects_id')
    #teaches = models.CharField(max_length=254, default='')
    # From student
    grade = models.ForeignKey(to='Grade', null=True, on_delete=models.SET_NULL, related_name='grade_id')
    # From staff
    staff_role = models.CharField(max_length=128, default='')
    schedule = models.ForeignKey(to='WeekSchedule', null=True, on_delete=models.SET_NULL, related_name='o_schedule')
    def activate(self):
        self.activated = True

    def show_self_grades(self):
        g = self.self_grades.all()
        mas = str()
        for i in range(len(g)):
            mas+= "<a href='../../school/" + str(g[i].grade_number) + '/' + str(g[i].grade_alpha) + "/'>" + str(g[i].grade_number) + str(g[i].grade_alpha) + '</a> '
        return mas

    def to_teach(self):
        all = Lesson_instance.objects.filter(lecturer=self)
        s = set()
        for i in all:
            s.add(i.name)
        return str(list(s)).replace('[', '').replace(']', '').replace("'", '')

    def full_name(self):
        return self.last_name + ' ' + self.first_name+' '+self.midname

class Grade(models.Model):
    grade_number = models.IntegerField(default=0)
    grade_alpha = models.CharField(max_length=1, default='')
    head = models.ForeignKey(to='MyUser', null=True, on_delete=models.SET_NULL, related_name='head_id')
    #elder = models.ForeignKey(to='MyUser', null=True, on_delete=models.SET_NULL, related_name='elder_student_id')
    roster = models.ManyToManyField(to='MyUser', related_name="roster")
    schedule = models.ForeignKey(to='WeekSchedule', null=True, on_delete=models.SET_NULL, related_name='schedule')

    def get_roster(self):
        r = self.roster.all()
        mas = []
        for i in r:
            mas.append(i)
        return mas

class Lesson(models.Model):
    name = models.CharField(max_length=128)


class Lesson_instance(models.Model):
    lesson = models.ForeignKey(to='Lesson', null=True, on_delete=models.SET_NULL, related_name='lesson_link')
    name = models.CharField(max_length=128, default='')
    lecturer = models.ForeignKey(to='MyUser', null=True, on_delete=models.SET_NULL, related_name='lecturer_id')
    grade_to_teach = models.ForeignKey(to='Grade', null=True, on_delete=models.SET_NULL,
                                       related_name='grade_to_teach_id')
    time = models.CharField(default='', max_length=4)
    additional_students = models.ManyToManyField(to='MyUser', related_name="additional_students")
    faculty = models.BooleanField(default=False)

    def get_lessons_names(self):
        l = Lesson_instance.objects.all()
        mas = []
        for i in l:
          mas.append(i.name)
        return set(mas)


    def get_additional_students(self):
        mas = []
        for i in self.additional_students.all():
            mas.append(i)

    def print_time(self):
        t = self.time
        r = t[:2] + ':' + t[2:]
        return r

'''
class Journal(models.Model):
    grade = models.ForeignKey(to='Grade', null=True, on_delete=models.SET_NULL, related_name='journal_of_grade')
    lesson = models.ForeignKey(to='Lesson_instance', null=True, on_delete=models.SET_NULL, related_name='journal_of_grade')
    days = models.ManyToManyField(to='Day', related_name="days_in_journal")

'''


class Journal(models.Model):
    to_user = models.ForeignKey(to='MyUser', null=True, on_delete=models.SET_NULL, related_name='journal_of_grade')
    lesson = models.ForeignKey(to='Lesson_instance', null=True, on_delete=models.SET_NULL, related_name='journal_of_grade')
    days = models.ManyToManyField(to='Day', related_name="days_in_journal")


class Day(models.Model):
    marks = models.ManyToManyField(to='Mark', related_name="roster")
    day = models.DateField(default=datetime.datetime.today().strftime('%Y-%m-%d'))
    # in_place = models.ManyToManyField(to='MyUser', related_name="roster_in_place")


class Mark(models.Model):
    # -1 if student is abscent
    value = models.IntegerField()
    day = models.DateField(default=datetime.datetime.today().strftime('%Y-%m-%d'))
    lesson = models.ForeignKey(to='Lesson_instance', null=True, on_delete=models.SET_NULL, related_name='mark_from_lesson')
    student = models.ForeignKey(to='MyUser', null=True, on_delete=models.SET_NULL, related_name='student')
    def get_days(stud):
        l = Mark.objects.filter(student = stud)
        mas = []
        for i in l:
          mas.append(i.day)
        return set(mas)



class DaySchedule(models.Model):
    day = models.IntegerField(default=0)
    # 1 - Monday, 2-Tuesday, 3-Wednesday, 4-Thursday, 5-Friday, 6-Saturday, 7-Sunday
    lessons = models.ManyToManyField(to='Lesson_instance', related_name='lessons_id')

    def my_lessons(self):
        l = []
        for i in self.lessons.all():
            l.append(i)
        return l
        
    def my_lessons_names(self):
        l = []
        for i in self.lessons.all():
            l.append(i.name)
        return l


class WeekSchedule(models.Model):
    all_days_schedules = models.ManyToManyField(to='DaySchedule', related_name='all_days_schedule')

    def days(self):
        l = []
        for i in self.all_days_schedules.all():
            l.append(i)
        return l