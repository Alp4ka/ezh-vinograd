from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
import django.contrib.auth
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.shortcuts import render
from .models import *
from django.contrib.auth.hashers import *
import hashlib
import datetime
from django.shortcuts import redirect
from django.http import HttpResponse, HttpResponseNotFound

value, lesson_id, student_id = map(int, input().split())
mark = Mark(value=value, lesson=Lesson_instance.objects.filter(id=lesson_id)[0], student=MyUser.objects.filter(id=student_id)[0])
mark.save