# Generated by Django 2.1.5 on 2020-03-12 19:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('journal', '0002_remove_myuser_info_parents'),
    ]

    operations = [
        migrations.AddField(
            model_name='myuser',
            name='info_parents',
            field=models.CharField(default='', max_length=228),
        ),
    ]
