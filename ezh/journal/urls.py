from django.urls import path

from . import views
app_name = 'journal'
urlpatterns = [
    path('', views.index, name='index'),
    path('add_mark/', views.add_mark, name='add_mark'),
    path('login/', views.login, name='login'),
    path('registration/', views.registration, name='register'),
    path('uid/', views.create_UID, name='uid'),
    path('school/roster/', views.school_roster, name='school_roster'),
    path('marks/', views.marks, name='marks'),
    path('<int:userid>/marks/', views.foreigner_marks, name='foreigner_marks'),
    path('profile/', views.profile, name='profile'),
    path('init/', views.initiate_test_data, name='init'),
    path('<int:userid>/profile/', views.foreigner_profile, name='foreigner_profile'),
    path('profile/edit/', views.profile_edit, name='profile_edit'),
    path('school/my_schedule/', views.my_schedule, name='my_schedule'),
    path('school/<int:class_n>/<str:class_a>/roster/', views.class_roster, name='class_roster'),
    path('school/<int:userid>/schedule/', views.foreigner_schedule, name='foreigner_schedule'),
    path('school/<int:class_n>/<str:class_a>/schedule/', views.class_schedule, name='class_schedule'),
    path('school/<int:class_n>/<str:class_a>/', views.class_info, name='class_info'),
    path('school/<int:class_n>/<str:class_a>/<int:lesson_id>', views.class_lesson_marks, name='class_lesson_marks'),
]
