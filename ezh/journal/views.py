from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
import django.contrib.auth
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.shortcuts import render
from .models import *
from django.contrib.auth.hashers import *
import hashlib
import datetime
from django.shortcuts import redirect
from django.http import HttpResponse, HttpResponseNotFound
import smtplib


@login_required(login_url='/login')
def school_roster(request):
    context = dict()
    context['teachers'] = MyUser.objects.filter(type_user=1)
    context['students'] = MyUser.objects.filter(type_user=2)
    context['staff'] = MyUser.objects.filter(type_user=3)
    return render(request, 'school_roster.html', context)


def add_mark(request):
    context = dict()
    if request.method == 'POST':
        value = request.POST['mark']
        lesson_id = request.POST['lesson_id']
        student_id = request.POST['student_id']
        mark = Mark(value=value, lesson=Lesson_instance.objects.filter(id=lesson_id)[0],
                    student=MyUser.objects.filter(id=student_id)[0])
        mark.save()

        current_date = datetime.datetime.today().strftime('%Y-%m-%d')
        day = Day.objects.filter(day=current_date)
        if len(day) == 0 or day is None:
            day = Day()
            day.save()
            day.marks.add(mark)
            day.save()
        else:
            day = Day.objects.filter(day=current_date)[0]
            day.marks.add(mark)
            day.save()

        journal = Journal.objects.filter(to_user=MyUser.objects.filter(id=student_id)[0],
                                         lesson=Lesson_instance.objects.filter(id=lesson_id)[0])
        if len(journal) == 0 or journal is None:
            journal = Journal(to_user=MyUser.objects.filter(id=student_id)[0],
                              lesson=Lesson_instance.objects.filter(id=lesson_id)[0])
            journal.save()
            journal.days.add(day)
            journal.save()
        else:
            if not day in journal[0].days.all():
                journal[0].days.add(day)
                journal[0].save()
    return render(request, 'add_mark.html', context)


@login_required(login_url='/login')
def class_roster(request, class_n, class_a):
    context = dict()
    grade = Grade.objects.filter(grade_alpha=class_a, grade_number=class_n)[0]
    context['grade'] = grade
    context['head'] = grade.head
    context['roster'] = grade.get_roster()
    return render(request, 'class_roster.html', context)


@login_required(login_url='/login')
def my_schedule(request):
    context = dict()
    context['my_profile'] = True
    user = request.user
    if user.type_user == 2:
        my_grade = user.grade
        if user.schedule:
            faculty_week_sch = sorted(user.schedule.days(), key=lambda x: x.day)
        else:
            faculty_week_sch = list()
        if my_grade.schedule:
            grade_week_sch = sorted(my_grade.schedule.days(), key=lambda x: x.day)
        else:
            grade_week_sch = list()
        result = [list()] * 6

        for i in grade_week_sch:
            result[i.day - 1] = sorted(i.my_lessons(), key=lambda x: x.time)
        for i in faculty_week_sch:
            result[i.day - 1].extend(sorted(i.my_lessons(), key=lambda x: x.time))
        context['moday'] = result[0]
        context['tuday'] = result[1]
        context['weday'] = result[2]
        context['thday'] = result[3]
        context['frday'] = result[4]
        context['saday'] = result[5]

        for i in result:
            print(i)
            print()
    elif user.type_user == 1:
        result = [list()] * 6
        monday = DaySchedule.objects.filter(day=1)
        lessons_monday = []
        need_monday = []
        for i in monday:
            lessons_monday.extend(i.my_lessons())

        print(lessons_monday)
        for i in lessons_monday:
            if i.lecturer == user:
                need_monday.append(i)
        print(need_monday)
        result[0] = sorted(need_monday, key=lambda x: x.time)

        monday = DaySchedule.objects.filter(day=2)
        lessons_monday = []
        need_monday = []
        for i in monday:
            lessons_monday.extend(i.my_lessons())
        for i in lessons_monday:
            if i.lecturer == user:
                need_monday.append(i)
        result[1] = sorted(need_monday, key=lambda x: x.time)

        monday = DaySchedule.objects.filter(day=3)
        lessons_monday = []
        need_monday = []
        for i in monday:
            lessons_monday.extend(i.my_lessons())
        for i in lessons_monday:
            if i.lecturer == user:
                need_monday.append(i)
        result[2] = sorted(need_monday, key=lambda x: x.time)

        monday = DaySchedule.objects.filter(day=4)
        lessons_monday = []
        need_monday = []
        for i in monday:
            lessons_monday.extend(i.my_lessons())
        for i in lessons_monday:
            if i.lecturer == user:
                need_monday.append(i)
        result[3] = sorted(need_monday, key=lambda x: x.time)

        monday = DaySchedule.objects.filter(day=5)
        lessons_monday = []
        need_monday = []
        for i in monday:
            lessons_monday.extend(i.my_lessons())
        for i in lessons_monday:
            if i.lecturer == user:
                need_monday.append(i)
        result[4] = sorted(need_monday, key=lambda x: x.time)

        monday = DaySchedule.objects.filter(day=6)
        lessons_monday = []
        need_monday = []
        for i in monday:
            lessons_monday.extend(i.my_lessons())
        for i in lessons_monday:
            if i.lecturer == user:
                need_monday.append(i)
        result[5] = sorted(need_monday, key=lambda x: x.time)

        context['moday'] = result[0]
        context['tuday'] = result[1]
        context['weday'] = result[2]
        context['thday'] = result[3]
        context['frday'] = result[4]
        context['saday'] = result[5]
    return render(request, 'my_schedule.html', context)


@login_required(login_url='/login')
def foreigner_schedule(request, userid):
    context = dict()
    context['my_profile'] = False
    user = MyUser.objects.filter(id=userid)[0]
    context['touser'] = user
    if user.type_user == 2:
        my_grade = user.grade
        if my_grade:
            if user.schedule:
                faculty_week_sch = sorted(user.schedule.days(), key=lambda x: x.day)
            else:
                faculty_week_sch = list()
            if my_grade.schedule:
                grade_week_sch = sorted(my_grade.schedule.days(), key=lambda x: x.day)
            else:
                grade_week_sch = list()
            result = [list()] * 6

            for i in grade_week_sch:
                result[i.day - 1] = sorted(i.my_lessons(), key=lambda x: x.time)
            for i in faculty_week_sch:
                result[i.day - 1].extend(sorted(i.my_lessons(), key=lambda x: x.time))
            context['moday'] = result[0]
            context['tuday'] = result[1]
            context['weday'] = result[2]
            context['thday'] = result[3]
            context['frday'] = result[4]
            context['saday'] = result[5]

            for i in result:
                print(i)
                print()
    elif user.type_user == 1:
        result = [list()] * 6
        monday = DaySchedule.objects.filter(day=1)
        lessons_monday = []
        need_monday = []
        for i in monday:
            lessons_monday.extend(i.my_lessons())
        for i in lessons_monday:
            if i.lecturer == user:
                need_monday.append(i)
        result[0] = sorted(need_monday, key=lambda x: x.time)

        monday = DaySchedule.objects.filter(day=2)
        lessons_monday = []
        need_monday = []
        for i in monday:
            lessons_monday.extend(i.my_lessons())
        for i in lessons_monday:
            if i.lecturer == user:
                need_monday.append(i)
        result[1] = sorted(need_monday, key=lambda x: x.time)

        monday = DaySchedule.objects.filter(day=3)
        lessons_monday = []
        need_monday = []
        for i in monday:
            lessons_monday.extend(i.my_lessons())
        for i in lessons_monday:
            if i.lecturer == user:
                need_monday.append(i)
        result[2] = sorted(need_monday, key=lambda x: x.time)

        monday = DaySchedule.objects.filter(day=4)
        lessons_monday = []
        need_monday = []
        for i in monday:
            lessons_monday.extend(i.my_lessons())
        for i in lessons_monday:
            if i.lecturer == user:
                need_monday.append(i)
        result[3] = sorted(need_monday, key=lambda x: x.time)

        monday = DaySchedule.objects.filter(day=5)
        lessons_monday = []
        need_monday = []
        for i in monday:
            lessons_monday.extend(i.my_lessons())
        for i in lessons_monday:
            if i.lecturer == user:
                need_monday.append(i)
        result[4] = sorted(need_monday, key=lambda x: x.time)

        monday = DaySchedule.objects.filter(day=6)
        lessons_monday = []
        need_monday = []
        for i in monday:
            lessons_monday.extend(i.my_lessons())
        for i in lessons_monday:
            if i.lecturer == user:
                need_monday.append(i)
        result[5] = sorted(need_monday, key=lambda x: x.time)

        context['moday'] = result[0]
        context['tuday'] = result[1]
        context['weday'] = result[2]
        context['thday'] = result[3]
        context['frday'] = result[4]
        context['saday'] = result[5]

    return render(request, 'my_schedule.html', context)


@login_required(login_url='/login')
def class_info(request, class_n, class_a):
    context = dict()
    user = request.user
    grade = Grade.objects.filter(grade_number=class_n, grade_alpha=class_a)[0]
    if grade.schedule:
        grade_week_sch = sorted(grade.schedule.days(), key=lambda x: x.day)
    else:
        grade_week_sch = list()
    result = [list()] * 6

    for i in grade_week_sch:
        result[i.day - 1] = sorted(i.my_lessons(), key=lambda x: x.time)
    context['head'] = grade.head
    context['roster'] = grade.get_roster()
    context['moday'] = result[0]
    context['tuday'] = result[1]
    context['weday'] = result[2]
    context['thday'] = result[3]
    context['frday'] = result[4]
    context['saday'] = result[5]
    context['grade'] = grade
    context['user'] = user

    return render(request, 'class_info.html', context)


@login_required(login_url='/login')
def class_lesson_marks(request, class_n, class_a, lesson_id):
    context = dict()
    grade = Grade.objects.filter(grade_number=class_n, grade_alpha=class_a)[0]
    context['grade'] = grade
    l = Lesson.objects.filter(id=lesson_id)[0]
    lessons = Lesson_instance.objects.filter(lesson=l)
    students = [i for i in grade.roster.all()]
    result = dict()
    for i in students:
        marks = []
        for j in lessons:
            marks.extend([(mark.value if 2 <= mark.value <= 5 else 'Н') for mark in Mark.objects.filter(lesson=j, student=i)])
        result[str(i.id)] = marks
    print(result)
    context['subject'] = l
    context['result'] = result
    context['table_len'] = range(len(sorted(result.values(), key=lambda x: len(x))[-1]))
    context['lessons'] = [
        {"name": str(students[i].full_name()), 'marks': result[str(students[i].id)]} for i in range(len(students))]

    return render(request, 'class_lesson_marks.html', context)


@login_required(login_url='/login')
def class_schedule(request, class_n, class_a):
    context = dict()
    user = request.user
    grade = Grade.objects.filter(grade_number=class_n, grade_alpha=class_a)[0]
    if grade.schedule:
        grade_week_sch = sorted(grade.schedule.days(), key=lambda x: x.day)
    else:
        grade_week_sch = list()
    result = [list()] * 6

    for i in grade_week_sch:
        result[i.day - 1] = sorted(i.my_lessons(), key=lambda x: x.time)

    context['moday'] = result[0]
    context['tuday'] = result[1]
    context['weday'] = result[2]
    context['thday'] = result[3]
    context['frday'] = result[4]
    context['saday'] = result[5]
    context['grade'] = grade
    context['user'] = user
    for i in result:
        print(i)
        print()

    return render(request, 'class_schedule.html', context)


@login_required(login_url='/login')
def foreigner_profile(request, userid):
    context = dict()
    user = MyUser.objects.get(id=userid)
    context['my_profile'] = False
    context['touser'] = user
    context['grades'] = user.self_grades.all()
    if user.type_user == 0:
        context['userrole'] = 'Гость'
    elif user.type_user == 1:
        context['userrole'] = 'Учитель'
    elif user.type_user == 2:
        context['userrole'] = 'Ученик'
    elif user.type_user == 3:
        context['userrole'] = 'Staff'

    return render(request, 'profile.html', context)
    pass


def index(request):
    return render(request, 'index.html')


def initiate_default_teachers():
    info = [
        ['Артем', 'Игнатьев', 'Игнатьевич', '11.11.1801', None, None, 1],
        ['Лейла', 'Вуржмаидзе', 'Тыртырбеч', '11.11.1301', None, None, 1],
        ['Акакий', 'Ыгъ', 'Звыкович', '11.11.1934', None, None, 1],
        ['Г', 'Г', 'Г', '11.11.1941', None, None, 1],
    ]
    for i in range(len(info)):
        line = info[i][0] + info[i][1] + info[i][2] + info[i][3] + str(info[i][6])
        hash = hashlib.md5(bytes(line, 'utf-8'))
        user = MyUser.objects.create_user(personal_code=str(hash.hexdigest()),
                                          username=str(hash.hexdigest()),
                                          first_name=info[i][0], type_user=1,
                                          last_name=info[i][1], midname=info[i][2],
                                          date_of_birth=datetime.datetime.strptime(info[i][3], "%d.%m.%Y"))
        user.activate()
        user.password = make_password('qwerty123')
        user.save()


def initiate_test_roster():
    info = [
        ['Kristina', 'Brittal', 'Otchestvo', '11.11.1991', 11, 'В', 2],
        ['Nikita', 'Sergeev', 'Otchestvo', '11.05.2002', 11, 'В', 2],
        ['Artem', 'Pechenkov', 'Otchestvo', '02.03.2004', 11, 'В', 2],
        ['Roman', 'Gorkov', 'Otchestvo', '12.02.2007', 11, 'В', 2],
        ['Andrei', 'Noskin', 'Otchestvo', '11.11.1973', None, None, 1]
    ]
    grade = Grade.objects.filter(grade_number=11, grade_alpha='В')[0]
    for i in range(len(info) - 1):
        line = info[i][0] + info[i][1] + info[i][2] + str(info[i][4]) + info[i][5] + info[i][3] + str(info[i][6])
        hash = hashlib.md5(bytes(line, 'utf-8'))
        user = MyUser.objects.create_user(personal_code=str(hash.hexdigest()), username=str(hash.hexdigest()),
                                          first_name=info[i][0], type_user=2,
                                          last_name=info[i][1], midname=info[i][2], grade=grade,
                                          date_of_birth=datetime.datetime.strptime(info[i][3],
                                                                                   "%d.%m.%Y"))
        user.activate()
        user.password = make_password('qwerty123')
        user.save()
        grade.roster.add(user)
        grade.save()
    line = info[-1][0] + info[-1][1] + info[-1][2] + info[-1][3] + str(info[-1][6])
    hash = hashlib.md5(bytes(line, 'utf-8'))
    user = MyUser.objects.create_user(personal_code=str(hash.hexdigest()), username=str(hash.hexdigest()),
                                      first_name=info[-1][0], type_user=1,
                                      last_name=info[-1][1], midname=info[-1][2],
                                      date_of_birth=datetime.datetime.strptime(info[-1][3],
                                                                               "%d.%m.%Y"))
    user.self_grades.add(grade)
    user.activate()
    user.password = make_password('qwerty123')
    user.save()
    grade.head = user
    grade.save()


def initiate_test_schedule():
    a = ['Информатика', 'Алгебра', 'Геометрия', 'Физика', 'История']
    for i in a:
        lesson = Lesson(name=i)
        lesson.save()
    l = Lesson_instance(lesson=Lesson.objects.get(name='Информатика'), name='Информатика',
                        lecturer=MyUser.objects.get(first_name='Артем'),
                        grade_to_teach=Grade.objects.get(grade_number=11, grade_alpha='В'),
                        time='0900')
    l.save()

    l1 = Lesson_instance(lesson=Lesson.objects.get(name='Информатика'), name='Информатика',
                         lecturer=MyUser.objects.get(first_name='Артем'),
                         grade_to_teach=Grade.objects.get(grade_number=11, grade_alpha='В'),
                         time='0955')
    l1.save()
    l2 = Lesson_instance(lesson=Lesson.objects.get(name='Алгебра'), name='Алгебра',
                         lecturer=MyUser.objects.get(first_name='Акакий'),
                         grade_to_teach=Grade.objects.get(grade_number=11, grade_alpha='В'),
                         time='1050')
    l2.save()
    l = Lesson_instance.objects.get(grade_to_teach=Grade.objects.get(grade_number=11, grade_alpha='В'),
                                    time='0900')
    print(l.time)
    d_s = DaySchedule(day=1)
    d_s.save()
    d_s.lessons.add(l)
    l = Lesson_instance.objects.get(grade_to_teach=Grade.objects.get(grade_number=11, grade_alpha='В'),
                                    time='0955')
    d_s.lessons.add(l)
    l = Lesson_instance.objects.get(grade_to_teach=Grade.objects.get(grade_number=11, grade_alpha='В'),
                                    time='1050')
    d_s.lessons.add(l)
    d_s.save()
    w_s = WeekSchedule()
    w_s.save()
    w_s.all_days_schedules.add(d_s)
    w_s.save()
    g = Grade.objects.get(grade_number=11, grade_alpha='В')
    g.schedule = w_s
    g.save()


@login_required(login_url='/login')
def initiate_test_data(request):
    if request.user.is_superuser:
        initiate_classes()
        initiate_default_teachers()
        initiate_test_schedule()
        initiate_test_roster()
        return HttpResponse('<h1>Тестовые данные загружены в бд</h1>')
    return HttpResponse(u'Куда прёшь?')


def initiate_classes():
    for i in range(5, 12):
        for j in 'АБВ':
            grade = Grade(grade_number=i, grade_alpha=j)
            grade.save()


@login_required(login_url='/login')
def create_UID(request):
    if request.user.is_superuser:
        context = dict()
        form = request.POST
        if request.method == 'POST':
            line = str(form['name']) + str(form['surname']) + str(form['mname']) + str(form['grade_n']) + str(
                form['grade_a']) + str(form['birthdate']) + str(form['role'])
            hash = hashlib.md5(bytes(line, 'utf-8'))
            if form['role'] == 'Ученик':
                grade = Grade.objects.filter(grade_number=form['grade_n'], grade_alpha=form['grade_a'])
                if len(grade) > 0:
                    grade = grade[0]
                    # date of birth забыл
                    user = MyUser.objects.create_user(personal_code=str(hash.hexdigest()),
                                                      username=str(hash.hexdigest()),
                                                      first_name=form['name'], type_user=2,
                                                      last_name=form['surname'], midname=form['mname'], grade=grade,
                                                      date_of_birth=datetime.datetime.strptime(form['birthdate'],
                                                                                               "%d.%m.%Y"))
                    user.save()
                    grade.roster.add(user)
                    grade.save()
            elif form['role'] == 'Учитель':
                grade = Grade.objects.filter(grade_number=form['grade_n'], grade_alpha=form['grade_a'])
                if len(grade) > 0:
                    grade = grade[0]
                    user = MyUser.objects.create_user(personal_code=str(hash.hexdigest()), first_name=form['name'],
                                                      username=str(hash.hexdigest()), type_user=1,
                                                      last_name=form['surname'], midname=form['mname'],
                                                      date_of_birth=datetime.datetime.strptime(form['birthdate'],
                                                                                               "%d.%m.%Y"))
                    user.save()
                    user.self_grades.add(grade)
                    user.save()
                    grade.head = user
                    grade.save()
            elif form['role'] == 'Staff':
                user = MyUser.objects.create_user(personal_code=str(hash.hexdigest()), first_name=form['name'],
                                                  username=str(hash.hexdigest()), type_user=3,
                                                  staff_role=form['staff_role'],
                                                  last_name=form['surname'], midname=form['mname'],
                                                  date_of_birth=datetime.datetime.strptime(form['birthdate'],
                                                                                           "%d.%m.%Y"))
                user.save()
            elif form['role'] == 'Гость':
                user = MyUser.objects.create_user(personal_code=str(hash.hexdigest()), first_name=form['name'],
                                                  username=str(hash.hexdigest()), type_user=0,
                                                  last_name=form['surname'], midname=form['mname'],
                                                  date_of_birth=datetime.datetime.strptime(form['birthdate'],
                                                                                           "%d.%m.%Y"))
                user.save()
            print(hash.hexdigest())
            mtpObj = smtplib.SMTP('smtp.mail.ru', 587)
            mtpObj.starttls()
            mtpObj.login('test_alp4ka@mail.ru', 'bcvjjvn02ii')
            r = ''
            if form['role'] == 'Гость':
                r = 'Guest'
            elif form['role'] == 'Staff':
                r = 'Staff'
            elif form['role'] == 'Учитель':
                r = 'Teacher'
            elif form['role'] == 'Ученик':
                r = 'Student'
            mtpObj.sendmail("test_alp4ka@mail.ru", "kisame2002@mail.ru", ('Your UID: ' + str(
                hash.hexdigest()) + ' \nCopy this code and paste it on registration page. Your role in EZH: ' + r + '!').encode(
                'ascii', 'ignore'))
            mtpObj.quit()
        return render(request, 'create_uid.html', context)
    else:
        return HttpResponseNotFound('<h1>Page not found</h1>')


def registration(request):
    context = dict()
    context['cname'] = request.user
    form = request.POST
    if request.method == 'POST':
        form = request.POST
        if len(MyUser.objects.filter(personal_code=form['uid_field'])) == 1:
            if not MyUser.objects.filter(personal_code=form['uid_field'])[0].activated:
                if len(form['username_field']) > 0 and form['username_field'] != \
                        MyUser.objects.filter(personal_code=form['uid_field'])[0].username:
                    if len(MyUser.objects.filter(username=form['username_field'])) == 0:
                        if form['password1_field'] == form['password2_field'] and len(form['password1_field']) > 7:
                            user = MyUser.objects.get(personal_code=form['uid_field'])
                            user.username = form['username_field']
                            user.password = make_password(form['password1_field'])
                            user.activate()
                            user.save()
                            print()
                            auth = authenticate(request, username=form['username_field'],
                                                password=form['password1_field'])
                            if auth is not None:
                                django.contrib.auth.login(request, auth)
                                context['message'] = 'Вход выполнен!'
                            else:
                                print('dolboeb')
                        elif form['password1_field'] != form['password2_field']:
                            context['message'] = 'Пароли не совпадают'
                            return render(request, 'registration.html', context)
                        elif len(form['password1_field']) <= 7:
                            context['message'] = 'Придумайте пароль подлинннее'
                    else:
                        context['message'] = 'Такой логин уже есть :('
                else:
                    context['message'] = 'Введите логин!'
            else:
                context['message'] = 'Пользователь уже активирован! Вам нужно лишь войти!'
        else:
            context['message'] = 'Такого UID не существует!'
    else:
        form = request.POST
    context['form'] = form
    return render(request, 'registration.html', context)


def login(request):
    context = dict()
    if request.method == 'POST':
        form = request.POST
        if not MyUser.objects.filter(username=form['username'])[0].activated:
            context['message'] = 'Вам нужно активировать пользователя с помощью UID!'
        else:
            user = authenticate(request, username=form['username'], password=form['password'])
            if user is not None:
                django.contrib.auth.login(request, user)
                context['message'] = 'Вход выполнен!'
                return HttpResponseRedirect('/')
            else:
                context['message'] = 'Не получилось войти:('
    else:
        form = request.POST

    context['form'] = form
    return render(request, 'login.html', context)


@login_required(login_url='/login')
def profile(request):
    context = dict()
    user = MyUser.objects.get(username=request.user)
    context['my_profile'] = True
    context['touser'] = user
    context['grades'] = user.self_grades.all()
    if user.type_user == 0:
        context['userrole'] = 'Гость'
    elif user.type_user == 1:
        context['userrole'] = 'Учитель'
    elif user.type_user == 2:
        context['userrole'] = 'Ученик'
    elif user.type_user == 3:
        context['userrole'] = 'Staff'
    if 'logout' in request.POST:
        logout(request)
        return redirect('/login')
    return render(request, 'profile.html', context)


@login_required(login_url='/login')
def profile_edit(request):
    context = dict()
    user = MyUser.objects.get(username=request.user)
    context['my_profile'] = True
    context['user'] = user
    if request.method == 'POST':
        tel = request.POST['telephone']
        email = request.POST['email']
        user.telephone = tel
        user.email = email
        user.save()
        return redirect('/profile/')
    return render(request, 'profile_edit.html', context)


@login_required(login_url='/login')
def foreigner_marks(request, userid):
    context = dict()
    context['my_profile'] = False
    user = MyUser.objects.get(id=userid)
    context['touser'] = user
    marks = Mark.objects.filter(student=user)

    if user.schedule:
        faculty_week_sch = sorted(user.schedule.days(), key=lambda x: x.day)
    else:
        faculty_week_sch = list()
    if user.grade.schedule:
        grade_week_sch = sorted(user.grade.schedule.days(), key=lambda x: x.day)
    else:
        grade_week_sch = list()
    l = []
    for i in grade_week_sch:
        l.extend(i.my_lessons_names())
    for i in faculty_week_sch:
        l.extend(i.my_lessons_names())
    l = list(set(l))
    mas = []

    for i in grade_week_sch:
        mas.extend(i.my_lessons())
    for i in faculty_week_sch:
        mas.extend(i.my_lessons())
    mas = list(set(mas))
    res = {str(i): [] for i in l}
    # print(mas)
    for i in range(len(mas)):
        marks = [(i.value if 2 <= i.value <= 5 else 'Н') for i in Mark.objects.filter(lesson=mas[i], student=user)]
        res[str(mas[i].name)].extend(marks)
    print(l)
    print(res)
    print(res["Физика"])
    print()
    context['table_len'] = range(len(sorted(res.values(), key=lambda x: len(x))[-1]))

    lessons = [{"name": l[i], 'marks': res[l[i]]} for i in range(len(l))]
    for i in lessons:
        print("i: ", i)
    days = Mark.get_days(stud=user)
    print(days)
    context['marks'] = marks
    context['lessons'] = lessons
    context['days'] = days

    return render(request, 'marks_table.html', context)


@login_required(login_url='/login')
def marks(request):
    context = dict()
    context['my_profile'] = True
    user = MyUser.objects.get(username=request.user)
    marks = Mark.objects.filter(student=user)

    if user.schedule:
        faculty_week_sch = sorted(user.schedule.days(), key=lambda x: x.day)
    else:
        faculty_week_sch = list()
    if user.grade.schedule:
        grade_week_sch = sorted(user.grade.schedule.days(), key=lambda x: x.day)
    else:
        grade_week_sch = list()
    l = []
    for i in grade_week_sch:
        l.extend(i.my_lessons_names())
    for i in faculty_week_sch:
        l.extend(i.my_lessons_names())
    l = list(set(l))
    mas = []

    for i in grade_week_sch:
        mas.extend(i.my_lessons())
    for i in faculty_week_sch:
        mas.extend(i.my_lessons())
    mas = list(set(mas))
    res = {str(i): [] for i in l}
    # print(mas)
    for i in range(len(mas)):
        marks = [(i.value if 2 <= i.value <= 5 else 'Н') for i in Mark.objects.filter(lesson=mas[i], student=user)]
        res[str(mas[i].name)].extend(marks)
    print(l)
    print(res)
    print(res["Физика"])
    print()
    context['table_len'] = range(len(sorted(res.values(), key=lambda x: len(x))[-1]))

    lessons = [{"name": l[i], 'marks': res[l[i]]} for i in range(len(l))]
    for i in lessons:
        print("i: ", i)
    days = Mark.get_days(stud=user)
    print(days)
    context['marks'] = marks
    context['lessons'] = lessons
    context['days'] = days

    return render(request, 'marks_table.html', context)
